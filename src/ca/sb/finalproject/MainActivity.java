package ca.sb.finalproject;
// -----------------------------------
import com.activeandroid.query.Select;

import android.os.Bundle;
import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.view.Menu;
// ----------------------
import ca.sb.finalproject.animating.*;
import ca.sb.finalproject.model.Stats;
// -------------------------------------------
public class MainActivity extends Activity
{
    public static MainActivity activity;
    public static Stats currentPlayer;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // ---------------------------------
        long playerId = this.getIntent().getExtras().getLong("id");
        currentPlayer = new Select().all().
            from(Stats.class).where("Id = ?",playerId).executeSingle();
        activity = this;//R.layout.activity_main);
        // -------------------------------------
        setContentView(new GamePanel(this)); 
    }
    public void onResume()
    {
      super.onResume();
      GamePanel.thread.setRunning(true);
    }
    public void onPause()
    {
      currentPlayer.save();
      GamePanel.thread.setRunning(false);//stop();
      super.onPause();
    }
    public void onDestroy()
    {
      currentPlayer.save();
      GamePanel.thread.setRunning(false);
      super.onDestroy();
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}