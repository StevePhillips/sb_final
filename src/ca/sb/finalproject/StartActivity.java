package ca.sb.finalproject;

import java.util.List;

import ca.sb.finalproject.model.StatListAdapter;
import ca.sb.finalproject.model.Stats;

import com.activeandroid.query.Select;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;

public class StartActivity extends Activity
{
  ListView listView;
  List<Stats> players;
  StatListAdapter playerListAdapter;
  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_start);
    listView = (ListView) findViewById(R.id.listView1);
    players = new Select().all().from(Stats.class).execute();
    playerListAdapter = new StatListAdapter(this, players);
    listView.setAdapter(playerListAdapter);
  }

  public void onNewPlayerClicked(View v)
  {
    Intent gameIntent = new Intent(this, NewPlayerActivity.class);
    startActivity(gameIntent);
  }
  public void onResume()
  {
    super.onResume();
    players = new Select().all().from(Stats.class).execute();
    playerListAdapter.setList(players);
    playerListAdapter.notifyDataSetChanged();
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu)
  {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.start, menu);
    return true;
  }

}
