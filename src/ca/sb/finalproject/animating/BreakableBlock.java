package ca.sb.finalproject.animating;

import ca.sb.finalproject.MainActivity;
import android.graphics.BitmapFactory;

public class BreakableBlock extends AnimatedSprite
{
  public BreakableBlock(int resourceId, int posX, int posY)
  {
    super(BitmapFactory.decodeResource(
            MainActivity.activity.getResources(),
            resourceId),
          posX, posY,
          32, 32,
          0, 2);
    setFramePeriod(0);
  }
  @Override
  public void update(long gameTime)
  {
    if(getFramePeriod()==0)
      return;
    if(getCurrentFrame()==1)
      die();
    super.update(gameTime);
  }
  public void crumble()
  {
    MainActivity.currentPlayer.blocksDestroyed++;
    this.setFramePeriod(1000);
  }
}
