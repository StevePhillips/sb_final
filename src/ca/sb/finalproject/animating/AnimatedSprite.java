package ca.sb.finalproject.animating;
// -----------------------------------
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
// -------------------------
public abstract class AnimatedSprite
{
  // -----------------------------------------
  private Bitmap bitmap;
  protected Rect sourceRect;
  private int frameNr;
  private int currentFrame;
  private long frameTicker;
  private int framePeriod;
  // -----------------------
  private int spriteWidth;
  private int spriteHeight;
  // -----------------------
  protected int x;
  protected int y;
  // ----------------
  private boolean alive = true;
  // -----------------
  public AnimatedSprite(Bitmap bmp, int x, int y, int width, int height,
                        int fps, int frameCount)
  {
    this.bitmap = bmp;
    this.x = x;
    this.y = y;
    currentFrame = 0;
    frameNr = frameCount;
    spriteWidth = width;//bitmap.getWidth()/frameCount;
    spriteHeight = height;//bitmap.getHeight();
    sourceRect = new Rect(0,0,spriteWidth, spriteHeight);
    framePeriod = (fps !=0 ? 1000/fps : 0);
    frameTicker = 0L;
  }
  public boolean living() { return alive; }
  public void die() { alive = false; }
  public Bitmap getBitmap()
  {
    return bitmap;
  }
  public void setBitmap(Bitmap bmp)
  {
    this.bitmap = bmp;
  }
  public void setSourceRect(Rect srcRect)
  {
    sourceRect = srcRect;
  }
  public int getFrameNr()
  {
    return frameNr;
  }
  public void setFrameNr(int frameNr)
  {
    this.frameNr = frameNr;
  }
  public int getCurrentFrame()
  {
    return currentFrame;
  }
  public void setCurrentFrame(int currentFrame)
  {
    this.currentFrame = currentFrame;
  }
  public float getFramePeriod()
  {
    return framePeriod;
  }
  public void setFramePeriod(int framePeriod)
  {
    this.framePeriod = framePeriod;
  }
  public int getSpriteWidth()
  {
    return spriteWidth;
  }
  public void setSpriteWidth(int spriteWidth)
  {
    this.spriteWidth = spriteWidth;
  }
  public int getSpriteHeight()
  {
    return spriteHeight;
  }
  public void setSpriteHeight(int spriteHeight)
  {
    this.spriteHeight = spriteHeight;
  }
  public int getX()
  {
    return x;
  }
  public void setX(int x)
  {
    this.x = x;
  }
  public int getY()
  {
    return y;
  }
  public void setY(int y)
  {
    this.y = y;
  }
  public void update(long gameTime)
  {    
    // ----------------
    if(gameTime > frameTicker+framePeriod)
    {
      frameTicker = gameTime;
      // -------------------
      currentFrame++;
      if(currentFrame >= frameNr)
      {
        currentFrame = 0;
      }
    }
    this.sourceRect.left = currentFrame * spriteWidth;
    this.sourceRect.right = this.sourceRect.left + spriteWidth;
  }
  public void draw(Canvas canvas)
  {
    Rect destRect = new Rect(getX(), getY(), getX() + spriteWidth,
                             getY() + spriteHeight);
    canvas.drawBitmap(bitmap, sourceRect, destRect, null);
  }
}
